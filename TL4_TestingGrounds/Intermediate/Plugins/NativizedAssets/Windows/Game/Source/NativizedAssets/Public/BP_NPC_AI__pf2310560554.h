#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/AIController.h"
class UBlackboardComponent;
class AActor;
class UAIPerceptionComponent;
#include "BP_NPC_AI__pf2310560554.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/TL4Character/AI/BP_NPC_AI.BP_NPC_AI_C", OverrideNativeName="BP_NPC_AI_C"))
class ABP_NPC_AI_C__pf2310560554 : public AAIController
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="AIPerception"))
	UAIPerceptionComponent* bpv__AIPerception__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Enemy Key", Category="Default", OverrideNativeName="EnemyKey"))
	FName bpv__EnemyKey__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Blackboard Component", Category="Default", OverrideNativeName="Blackboard Component"))
	UBlackboardComponent* bpv__BlackboardxComponent__pfT;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_ComponentBoundEvent_Actor"))
	AActor* b0l__K2Node_ComponentBoundEvent_Actor__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_ComponentBoundEvent_Stimulus"))
	FAIStimulus b0l__K2Node_ComponentBoundEvent_Stimulus__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_UseBlackboard_BlackboardComponent"))
	UBlackboardComponent* b0l__CallFunc_UseBlackboard_BlackboardComponent__pf;
	ABP_NPC_AI_C__pf2310560554(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_BP_NPC_AI__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_NPC_AI__pf_1(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(OverrideNativeName="BndEvt__AIPerception_K2Node_ComponentBoundEvent_0_ActorPerceptionUpdatedDelegate__DelegateSignature"))
	virtual void bpf__BndEvt__AIPerception_K2Node_ComponentBoundEvent_0_ActorPerceptionUpdatedDelegate__DelegateSignature__pf(AActor* bpp__Actor__pf, FAIStimulus bpp__Stimulus__pf);
	UFUNCTION(meta=(DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Update Blackboard Enemy Key"))
	virtual void bpf__UpdatexBlackboardxEnemyxKey__pfTTT(AActor* bpp__ControlledActor__pf, FAIStimulus const& bpp__AIStimulus__pf__const, AActor* bpp__ObjectValue__pf);
public:
};
