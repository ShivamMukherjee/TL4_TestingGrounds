#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Animation/AnimClassData.h"
#include "Runtime/Engine/Classes/Animation/AnimNodeSpaceConversions.h"
#include "Runtime/AnimGraphRuntime/Public/BoneControllers/AnimNode_TwoBoneIK.h"
#include "Runtime/AnimGraphRuntime/Public/AnimNodes/AnimNode_Slot.h"
#include "Runtime/Engine/Classes/Animation/AnimNode_StateMachine.h"
#include "Runtime/AnimGraphRuntime/Public/AnimNodes/AnimNode_StateResult.h"
#include "Runtime/Engine/Classes/Animation/AnimNode_SequencePlayer.h"
#include "Runtime/Engine/Classes/Animation/AnimNode_TransitionResult.h"
#include "Runtime/AnimGraphRuntime/Public/AnimNodes/AnimNode_Root.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "ABP_FP__pf117350442.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/TL4Character/Animations/ABP_FP.ABP_FP_C", OverrideNativeName="ABP_FP_C"))
class UABP_FP_C__pf117350442 : public UAnimInstance
{
public:
	GENERATED_BODY()
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_Root_1DF598784F4A087A4ED10A805C775AC8"))
	FAnimNode_Root bpv__AnimGraphNode_Root_1DF598784F4A087A4ED10A805C775AC8__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_DA3192DC415202A934CDF598A41EFCDE"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_DA3192DC415202A934CDF598A41EFCDE__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_2C413F41496E16A9E28BB6886E13325E"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_2C413F41496E16A9E28BB6886E13325E__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_DF5EA38F40899E6FA34A95A078F6AC7B"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_DF5EA38F40899E6FA34A95A078F6AC7B__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_B368400946B18E8B82462BA54F4BB849"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_B368400946B18E8B82462BA54F4BB849__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_FD08A51A4E3E4A767C7ED6B9E0A8B8C8"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_FD08A51A4E3E4A767C7ED6B9E0A8B8C8__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_A3E5D0514472C54EE7856B9AD4BDD068"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_A3E5D0514472C54EE7856B9AD4BDD068__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TransitionResult_B15D7EAA46138A38EAF1CCAD3110ACB7"))
	FAnimNode_TransitionResult bpv__AnimGraphNode_TransitionResult_B15D7EAA46138A38EAF1CCAD3110ACB7__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_SequencePlayer_FC6F51DD47C7BDF0925AB6BC839E834B"))
	FAnimNode_SequencePlayer bpv__AnimGraphNode_SequencePlayer_FC6F51DD47C7BDF0925AB6BC839E834B__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_StateResult_D916F73945F6040112B2C1AAD24BA05E"))
	FAnimNode_StateResult bpv__AnimGraphNode_StateResult_D916F73945F6040112B2C1AAD24BA05E__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_SequencePlayer_6185C6CD492FBBA77199998F8E64BA49"))
	FAnimNode_SequencePlayer bpv__AnimGraphNode_SequencePlayer_6185C6CD492FBBA77199998F8E64BA49__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_StateResult_22EFEF3344C16D03398133927FDEB383"))
	FAnimNode_StateResult bpv__AnimGraphNode_StateResult_22EFEF3344C16D03398133927FDEB383__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_SequencePlayer_C6641E0144D1F470927AA89C762FE920"))
	FAnimNode_SequencePlayer bpv__AnimGraphNode_SequencePlayer_C6641E0144D1F470927AA89C762FE920__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_StateResult_7193846E40FEA5A17832388CFB6D5ECA"))
	FAnimNode_StateResult bpv__AnimGraphNode_StateResult_7193846E40FEA5A17832388CFB6D5ECA__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_SequencePlayer_BFF64F4B46D7BCDA664E36A93E4AC4AA"))
	FAnimNode_SequencePlayer bpv__AnimGraphNode_SequencePlayer_BFF64F4B46D7BCDA664E36A93E4AC4AA__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_StateResult_DEFCC83E421E50B38F25819E1EB93A5B"))
	FAnimNode_StateResult bpv__AnimGraphNode_StateResult_DEFCC83E421E50B38F25819E1EB93A5B__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_SequencePlayer_4869ECD8456855570F8CB5A7ECB06A59"))
	FAnimNode_SequencePlayer bpv__AnimGraphNode_SequencePlayer_4869ECD8456855570F8CB5A7ECB06A59__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_StateResult_ADB52C394181B8382543E7B06CC0E78F"))
	FAnimNode_StateResult bpv__AnimGraphNode_StateResult_ADB52C394181B8382543E7B06CC0E78F__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_StateMachine_885684DE4B0E2CE3646364873E1871B5"))
	FAnimNode_StateMachine bpv__AnimGraphNode_StateMachine_885684DE4B0E2CE3646364873E1871B5__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_Slot_3BF6EDDF4D71635E122E509C22B58E95"))
	FAnimNode_Slot bpv__AnimGraphNode_Slot_3BF6EDDF4D71635E122E509C22B58E95__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_TwoBoneIK_69BA3C444FB8533C25AA0BBA66C9EE20"))
	FAnimNode_TwoBoneIK bpv__AnimGraphNode_TwoBoneIK_69BA3C444FB8533C25AA0BBA66C9EE20__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_LocalToComponentSpace_AF1BAC5E49C121924DCE7B9291CC5365"))
	FAnimNode_ConvertLocalToComponentSpace bpv__AnimGraphNode_LocalToComponentSpace_AF1BAC5E49C121924DCE7B9291CC5365__pf;
	UPROPERTY(meta=(OverrideNativeName="AnimGraphNode_ComponentToLocalSpace_E527A26349C6D98A0F8497984D1FF555"))
	FAnimNode_ConvertComponentToLocalSpace bpv__AnimGraphNode_ComponentToLocalSpace_E527A26349C6D98A0F8497984D1FF555__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Is Moving", Category="Default", OverrideNativeName="IsMoving"))
	bool bpv__IsMoving__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Is In Air", Category="Default", OverrideNativeName="bIsInAir"))
	bool bpv__bIsInAir__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaTimeX"))
	float b0l__K2Node_Event_DeltaTimeX__pf;
	UABP_FP_C__pf117350442(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void __InitAllAnimNodes();
	void __InitAnimNode__AnimGraphNode_Root_1DF598784F4A087A4ED10A805C775AC8();
	void __InitAnimNode__AnimGraphNode_TransitionResult_DA3192DC415202A934CDF598A41EFCDE();
	void __InitAnimNode__AnimGraphNode_TransitionResult_2C413F41496E16A9E28BB6886E13325E();
	void __InitAnimNode__AnimGraphNode_TransitionResult_DF5EA38F40899E6FA34A95A078F6AC7B();
	void __InitAnimNode__AnimGraphNode_TransitionResult_B368400946B18E8B82462BA54F4BB849();
	void __InitAnimNode__AnimGraphNode_TransitionResult_FD08A51A4E3E4A767C7ED6B9E0A8B8C8();
	void __InitAnimNode__AnimGraphNode_TransitionResult_A3E5D0514472C54EE7856B9AD4BDD068();
	void __InitAnimNode__AnimGraphNode_TransitionResult_B15D7EAA46138A38EAF1CCAD3110ACB7();
	void __InitAnimNode__AnimGraphNode_SequencePlayer_FC6F51DD47C7BDF0925AB6BC839E834B();
	void __InitAnimNode__AnimGraphNode_StateResult_D916F73945F6040112B2C1AAD24BA05E();
	void __InitAnimNode__AnimGraphNode_SequencePlayer_6185C6CD492FBBA77199998F8E64BA49();
	void __InitAnimNode__AnimGraphNode_StateResult_22EFEF3344C16D03398133927FDEB383();
	void __InitAnimNode__AnimGraphNode_SequencePlayer_C6641E0144D1F470927AA89C762FE920();
	void __InitAnimNode__AnimGraphNode_StateResult_7193846E40FEA5A17832388CFB6D5ECA();
	void __InitAnimNode__AnimGraphNode_SequencePlayer_BFF64F4B46D7BCDA664E36A93E4AC4AA();
	void __InitAnimNode__AnimGraphNode_StateResult_DEFCC83E421E50B38F25819E1EB93A5B();
	void __InitAnimNode__AnimGraphNode_SequencePlayer_4869ECD8456855570F8CB5A7ECB06A59();
	void __InitAnimNode__AnimGraphNode_StateResult_ADB52C394181B8382543E7B06CC0E78F();
	void __InitAnimNode__AnimGraphNode_StateMachine_885684DE4B0E2CE3646364873E1871B5();
	void __InitAnimNode__AnimGraphNode_Slot_3BF6EDDF4D71635E122E509C22B58E95();
	void __InitAnimNode__AnimGraphNode_TwoBoneIK_69BA3C444FB8533C25AA0BBA66C9EE20();
	void __InitAnimNode__AnimGraphNode_LocalToComponentSpace_AF1BAC5E49C121924DCE7B9291CC5365();
	void __InitAnimNode__AnimGraphNode_ComponentToLocalSpace_E527A26349C6D98A0F8497984D1FF555();
	void bpf__ExecuteUbergraph_ABP_FP__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_ABP_FP__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_ABP_FP__pf_2(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(OverrideNativeName="EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_FP_AnimGraphNode_TransitionResult_FD08A51A4E3E4A767C7ED6B9E0A8B8C8"))
	virtual void bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_FP_AnimGraphNode_TransitionResult_FD08A51A4E3E4A767C7ED6B9E0A8B8C8__pf();
	UFUNCTION(meta=(ToolTip="Executed when the Animation is updated", CppFromBpEvent, OverrideNativeName="BlueprintUpdateAnimation"))
	virtual void bpf__BlueprintUpdateAnimation__pf(float bpp__DeltaTimeX__pf);
	UFUNCTION(meta=(OverrideNativeName="EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_FP_AnimGraphNode_TransitionResult_DF5EA38F40899E6FA34A95A078F6AC7B"))
	virtual void bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_FP_AnimGraphNode_TransitionResult_DF5EA38F40899E6FA34A95A078F6AC7B__pf();
public:
};
