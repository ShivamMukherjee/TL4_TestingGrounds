#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "TL4_TestingGrounds/TL4Character/Mannequin.h"
class APlayerController;
class UWBP_Resetting_C__pf3389175594;
class UArrowComponent;
class UPostProcessComponent;
class UBoxComponent;
class UPatrolRoute;
class UTimelineComponent;
class AController;
class UCharacterMovementComponent;
class UDamageType;
class AActor;
#include "BP_TL4Character__pf3043094308.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/TL4Character/Behaviour/BP_TL4Character.BP_TL4Character_C", OverrideNativeName="BP_TL4Character_C"))
class ABP_TL4Character_C__pf3043094308 : public AMannequin
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="DeathCameraSpawnPoint"))
	UArrowComponent* bpv__DeathCameraSpawnPoint__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="PostProcess"))
	UPostProcessComponent* bpv__PostProcess__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="EffectVolume"))
	UBoxComponent* bpv__EffectVolume__pf;
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="PatrolRoute"))
	UPatrolRoute* bpv__PatrolRoute__pf;
	UPROPERTY(meta=(OverrideNativeName="Damage_Fade_Blend_Weight_AEF775E04681EC7A1A49ACB60F6072C8"))
	float bpv__Damage_Fade_Blend_Weight_AEF775E04681EC7A1A49ACB60F6072C8__pf;
	UPROPERTY(meta=(OverrideNativeName="Damage_Fade__Direction_AEF775E04681EC7A1A49ACB60F6072C8"))
	TEnumAsByte<ETimelineDirection::Type> bpv__Damage_Fade__Direction_AEF775E04681EC7A1A49ACB60F6072C8__pf;
	UPROPERTY(BlueprintReadWrite, meta=(Category="BP_TL4Character", OverrideNativeName="Damage Fade"))
	UTimelineComponent* bpv__DamagexFade__pfT;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Jump Button Down", Category="Default", OverrideNativeName="Jump Button Down"))
	bool bpv__JumpxButtonxDown__pfTT;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Crouch Button Down", Category="Default", OverrideNativeName="Crouch Button Down"))
	bool bpv__CrouchxButtonxDown__pfTT;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Aiming", Category="Default", OverrideNativeName="Aiming"))
	bool bpv__Aiming__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Health", Category="Default", ClampMin="0", UIMin="0", OverrideNativeName="Health"))
	float bpv__Health__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Max Health", Category="Default", OverrideNativeName="MaxHealth"))
	float bpv__MaxHealth__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Old Controller", Category="Default", OverrideNativeName="Old Controller"))
	AController* bpv__OldxController__pfT;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Is Game Over", Category="Default", OverrideNativeName="Is Game Over"))
	bool bpv__IsxGamexOver__pfTT;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key3"))
	FKey b0l__K2Node_InputActionEvent_Key3__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key2"))
	FKey b0l__K2Node_InputActionEvent_Key2__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="Temp_struct_Variable"))
	FKey b0l__Temp_struct_Variable__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key1"))
	FKey b0l__K2Node_InputActionEvent_Key1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputActionEvent_Key"))
	FKey b0l__K2Node_InputActionEvent_Key__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="Temp_struct_Variable1"))
	FKey b0l__Temp_struct_Variable1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputKeyEvent_Key1"))
	FKey b0l__K2Node_InputKeyEvent_Key1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputKeyEvent_Key"))
	FKey b0l__K2Node_InputKeyEvent_Key__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_Create_ReturnValue"))
	UWBP_Resetting_C__pf3389175594* b0l__CallFunc_Create_ReturnValue__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsPlayer_Controller"))
	APlayerController* b0l__K2Node_DynamicCast_AsPlayer_Controller__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess"))
	bool b0l__K2Node_DynamicCast_bSuccess__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputAxisEvent_AxisValue3"))
	float b0l__K2Node_InputAxisEvent_AxisValue3__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputAxisEvent_AxisValue2"))
	float b0l__K2Node_InputAxisEvent_AxisValue2__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputAxisEvent_AxisValue1"))
	float b0l__K2Node_InputAxisEvent_AxisValue1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_BreakRotator_Roll"))
	float b0l__CallFunc_BreakRotator_Roll__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_BreakRotator_Pitch"))
	float b0l__CallFunc_BreakRotator_Pitch__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_BreakRotator_Yaw"))
	float b0l__CallFunc_BreakRotator_Yaw__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsCharacter_Movement_Component"))
	UCharacterMovementComponent* b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess1"))
	bool b0l__K2Node_DynamicCast_bSuccess1__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_Damage"))
	float b0l__K2Node_Event_Damage__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DamageType"))
	UDamageType* b0l__K2Node_Event_DamageType__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_InstigatedBy"))
	AController* b0l__K2Node_Event_InstigatedBy__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DamageCauser"))
	AActor* b0l__K2Node_Event_DamageCauser__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_InputAxisEvent_AxisValue"))
	float b0l__K2Node_InputAxisEvent_AxisValue__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="CallFunc_GetCurrentLevelName_ReturnValue"))
	FString b0l__CallFunc_GetCurrentLevelName_ReturnValue__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_NewController"))
	AController* b0l__K2Node_Event_NewController__pf;
	ABP_TL4Character_C__pf3043094308(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_2(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_3(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_4(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_5(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_6(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_7(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_8(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_9(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_10(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_TL4Character__pf_11(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(DisplayName="Possessed", ToolTip="Event called when the Pawn is possessed by a Controller (normally only occurs on the server/standalone).", CppFromBpEvent, OverrideNativeName="ReceivePossessed"))
	virtual void bpf__ReceivePossessed__pf(AController* bpp__NewController__pf);
	UFUNCTION(meta=(OverrideNativeName="InpAxisEvt_MoveForward_K2Node_InputAxisEvent_180"))
	virtual void bpf__InpAxisEvt_MoveForward_K2Node_InputAxisEvent_180__pf(float bpp__AxisValue__pf);
	UFUNCTION(BlueprintAuthorityOnly, meta=(Category="Game|Damage", DisplayName="AnyDamage", ToolTip="Event when this actor takes ANY damage", CppFromBpEvent, OverrideNativeName="ReceiveAnyDamage"))
	virtual void bpf__ReceiveAnyDamage__pf(float bpp__Damage__pf, const UDamageType* bpp__DamageType__pf__const, AController* bpp__InstigatedBy__pf, AActor* bpp__DamageCauser__pf);
	UFUNCTION(meta=(OverrideNativeName="InpAxisEvt_LookUp_K2Node_InputAxisEvent_268"))
	virtual void bpf__InpAxisEvt_LookUp_K2Node_InputAxisEvent_268__pf(float bpp__AxisValue__pf);
	UFUNCTION(meta=(OverrideNativeName="InpAxisEvt_Turn_K2Node_InputAxisEvent_256"))
	virtual void bpf__InpAxisEvt_Turn_K2Node_InputAxisEvent_256__pf(float bpp__AxisValue__pf);
	UFUNCTION(meta=(OverrideNativeName="InpAxisEvt_MoveRight_K2Node_InputAxisEvent_243"))
	virtual void bpf__InpAxisEvt_MoveRight_K2Node_InputAxisEvent_243__pf(float bpp__AxisValue__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_BackSpace_K2Node_InputKeyEvent_0"))
	virtual void bpf__InpActEvt_BackSpace_K2Node_InputKeyEvent_0__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_Gamepad_FaceButton_Right_K2Node_InputKeyEvent_1"))
	virtual void bpf__InpActEvt_Gamepad_FaceButton_Right_K2Node_InputKeyEvent_1__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_Crouch_K2Node_InputActionEvent_0"))
	virtual void bpf__InpActEvt_Crouch_K2Node_InputActionEvent_0__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_Crouch_K2Node_InputActionEvent_1"))
	virtual void bpf__InpActEvt_Crouch_K2Node_InputActionEvent_1__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_Jump_K2Node_InputActionEvent_2"))
	virtual void bpf__InpActEvt_Jump_K2Node_InputActionEvent_2__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="InpActEvt_Jump_K2Node_InputActionEvent_3"))
	virtual void bpf__InpActEvt_Jump_K2Node_InputActionEvent_3__pf(FKey bpp__Key__pf);
	UFUNCTION(meta=(OverrideNativeName="Damage Fade__UpdateFunc"))
	virtual void bpf__DamagexFade__UpdateFunc__pfT();
	UFUNCTION(meta=(OverrideNativeName="Damage Fade__FinishedFunc"))
	virtual void bpf__DamagexFade__FinishedFunc__pfT();
	UFUNCTION(BlueprintCallable, meta=(BlueprintInternalUseOnly="true", DisplayName="Construction Script", ToolTip="Construction script, the place to spawn components and do other setup.@note Name used in CreateBlueprint function@param       Location        The location.@param       Rotation        The rotation.", Category, CppFromBpEvent, OverrideNativeName="UserConstructionScript"))
	virtual void bpf__UserConstructionScript__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="Is Dead"))
	virtual void bpf__IsxDead__pfT(/*out*/ bool& bpp__Dead__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Take Damage"))
	virtual void bpf__TakexDamage__pfT(float bpp__Damage__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Kill Player"))
	virtual void bpf__KillxPlayer__pfT();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Spawn Death Camera"))
	virtual void bpf__SpawnxDeathxCamera__pfTT();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Render Game Over Menu"))
	virtual void bpf__RenderxGamexOverxMenu__pfTTT();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="CallDestroy"))
	virtual void bpf__CallDestroy__pf();
public:
};
