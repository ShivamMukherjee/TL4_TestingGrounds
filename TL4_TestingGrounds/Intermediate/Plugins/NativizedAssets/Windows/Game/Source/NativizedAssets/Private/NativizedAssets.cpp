// Copyright (c) Shivam Mukherjee 2017

#include "NativizedAssets.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

class FNativizedAssetsModule : public IModuleInterface
{	
	virtual void StartupModule() override
	{
			
	}
};

IMPLEMENT_MODULE( FNativizedAssetsModule, NativizedAssets );
